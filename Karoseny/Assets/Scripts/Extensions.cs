using UnityEngine;

namespace ManicGames
{
    public static class Extensions
    {
        public static float Remap(float value, float oldMin, float oldMax, float newMin, float newMax)
        {
            var normalized = (value - oldMin) / (oldMax - oldMin);
            return normalized * (newMax - newMin) + newMin;
        }

        public static Vector3 With(this Vector3 v, float? x = null, float? y = null, float? z = null)
        {
            if (x.HasValue) v.x = x.Value;
            if (y.HasValue) v.y = y.Value;
            if (z.HasValue) v.z = z.Value;
            return v;
        }

        public static Vector2 With(this Vector2 v, float? x = null, float? y = null)
        {
            if (x.HasValue) v.x = x.Value;
            if (y.HasValue) v.y = y.Value;
            return v;
        }

        public static bool RequiredBy<T>(this T obj, Object context)
            where T : Object
        {
            bool isMissing = (obj == null);
            if (isMissing)
                Debug.LogError($"<color=red>MISSING</color> - <color=yellow>{typeof(T)}</color> - on {context}",
                    context);

            return !isMissing;
        }
    }
}