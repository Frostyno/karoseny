using System;
using UnityEngine;

namespace ManicGames
{
    public class GroundChecker : MonoBehaviour
    {
        [SerializeField] float _groundCheckDepth = 0.3f;
        [SerializeField] LayerMask _groundMask = new LayerMask();

        Collider2D _collider = null;

        bool? _isGrounded = null;
        public bool IsGrounded
        {
            get
            {
                if (_isGrounded.HasValue)
                    return _isGrounded.Value;
                
                CheckGrounded();
                return _isGrounded ?? false;
            }
        }

        void Awake() => _collider = GetComponent<Collider2D>();

        void LateUpdate()
        {
            if (_isGrounded.HasValue)
                _isGrounded = null;
        }

        void CheckGrounded()
        {
            var corners = CalculateBoundsCorners();
            var ground = Physics2D.OverlapArea(corners.LeftBottom,
                new Vector2(corners.RightBottom.x, corners.RightBottom.y - _groundCheckDepth), _groundMask);
            _isGrounded = ground != null;
        }

        BoundsCorners CalculateBoundsCorners()
        {
            var bounds = _collider.bounds;
            Vector2 extents = bounds.extents;
            BoundsCorners corners = new BoundsCorners()
            {
                LeftBottom = (Vector2) bounds.center - extents,
                RightBottom = (Vector2) bounds.center + new Vector2(extents.x, -extents.y),
                LeftTop = (Vector2) bounds.center + new Vector2(-extents.x, extents.y),
                RightTop = (Vector2) bounds.center + extents
            };
            return corners;
        }

        void OnDrawGizmos()
        {
            if (_collider == null) return;

            var corners = CalculateBoundsCorners();
            Gizmos.DrawWireSphere(corners.LeftBottom, 0.1f);
            Gizmos.DrawWireSphere(corners.RightBottom, 0.1f);
        }
    }
}