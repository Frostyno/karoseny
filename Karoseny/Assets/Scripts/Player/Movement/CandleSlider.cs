using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ManicGames
{
    public class CandleSlider : MonoBehaviour, IMaxSpeedModifier
    {
        [SerializeField, Required] GroundChecker _groundChecker = null;
        [SerializeField, Required] Rigidbody2D _rigidbody = null;
        
        [SerializeField, Range(0f, 10f)] float _slideSpeedModifier = 5f;
        [SerializeField, Range(0f, 2f)] float _slideLength = 1f;

        bool _sliding = false;
        
        public float CurrentModifier { get; private set; } = 0f;

        public void Slide()
        {
            if (_sliding) return;

            _sliding = true;
            CurrentModifier = _slideSpeedModifier;
            _rigidbody.AddForce(new Vector2(Mathf.Sign(_rigidbody.velocity.x) * 1000f, 0f), ForceMode2D.Impulse);
            StartCoroutine(SlowSlideOverTime());
        }

        IEnumerator SlowSlideOverTime()
        {
            var slideDirection = Mathf.Sign(_rigidbody.velocity.x);
            var slowAmountPerSecond = _slideSpeedModifier / _slideLength;
            while (CurrentModifier > 0f)
            {   
                yield return null;
                if (slideDirection != Mathf.Sign(_rigidbody.velocity.x))
                {
                    CurrentModifier = 0f;
                    break;
                }
                
                if (!_groundChecker.IsGrounded)
                    continue;
                
                CurrentModifier -= slowAmountPerSecond * Time.deltaTime;
                CurrentModifier = Mathf.Max(CurrentModifier, 0f);
            }
            _sliding = false;
        }
    }
}