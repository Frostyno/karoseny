using System;
using UnityEngine;

namespace ManicGames
{
    public class PlayerFlyingMover : MonoBehaviour
    {
        [SerializeField] Vector2 _maxVelocity = new Vector2(3f, 3f);
        [SerializeField] Vector2 _stepStrength = new Vector2(10f, 10f);

        Rigidbody2D _rigidbody = null;
        InputManager _inputManager = null;
        KaroseneLampController _karoseneLampController = null;

        float _gravityScaleTemp = 0f;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _karoseneLampController = GetComponent<KaroseneLampController>();
        }

        void Start() => _inputManager = AppData.InputManager;

        void OnEnable()
        {
            _gravityScaleTemp = _rigidbody.gravityScale;
            _rigidbody.gravityScale = 0f;
        }

        void OnDisable()
        {
            _rigidbody.gravityScale = _gravityScaleTemp;
        }

        void FixedUpdate()
        {
            // var velocity = _rigidbody.velocity;
            // velocity.x = Mathf.Lerp(velocity.x, _inputManager.HorizontalInputRaw * _maxVelocity.x,
            //     _stepStrength.x * Time.fixedDeltaTime);
            // velocity.y = Mathf.Lerp(velocity.y, _maxVelocity.y * _karoseneLampController.FlameStrengthOneNormalized,
            //     _stepStrength.y * Time.fixedDeltaTime);
            // _rigidbody.velocity = velocity;
        }
    }
}