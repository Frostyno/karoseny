using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ManicGames
{
    public class ForceGroundMover : MonoBehaviour
    {
        [FoldoutGroup("Movement Settings")]
        [SerializeField, Range(1f, 500f)] float _accelerationForce = 100f;
        [FoldoutGroup("Movement Settings")]
        [SerializeField, Range(1f, 500f)] float _decelerationForce = 100f;
        [FoldoutGroup("Movement Settings")]
        [SerializeField, Range(1f, 30f)] float _maxHorizontalSpeed = 8f;
        [FoldoutGroup("Movement Settings")]
        [SerializeField, Range(1f, 30f)] float _maxFallingSpeed = 10f;
        [FoldoutGroup("Movement Settings")]
        [SerializeField, Range(1f, 50f)] float _jumpForce = 7f;
        
        Rigidbody2D _rigidbody = null;
        GroundChecker _groundChecker = null;
        List<IMaxSpeedModifier> _speedModifiers = null;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _groundChecker = GetComponent<GroundChecker>();
            _speedModifiers = GetComponents<IMaxSpeedModifier>().ToList();
        }

        public void Move(Vector2 movementInputRaw)
        {
            if (movementInputRaw.x != 0f)
                Accelerate(movementInputRaw.x);
            else if (_rigidbody.velocity.x != 0f)
                Decelerate();

            _rigidbody.velocity = _rigidbody.velocity.With(y: Mathf.Max(_rigidbody.velocity.y, -_maxFallingSpeed));
        }

        void Accelerate(float horizontalInputRaw)
        {
            var forceVector = new Vector2(horizontalInputRaw, 0f) * _accelerationForce;
            _rigidbody.AddForce(forceVector);

            var maxSpeed = _maxHorizontalSpeed;
            _speedModifiers.ForEach(m => maxSpeed += m.CurrentModifier);
            _rigidbody.velocity = _rigidbody.velocity.With(x: Mathf.Clamp(
                _rigidbody.velocity.x, -maxSpeed, maxSpeed));
        }

        void Decelerate()
        {
            if (_rigidbody.velocity.x < 0.2f)
            {
                _rigidbody.velocity = _rigidbody.velocity.With(x: 0f);
                return;
            }

            var forceVector = new Vector2(-Mathf.Sign(_rigidbody.velocity.x), 0f) * _decelerationForce;
            _rigidbody.AddForce(forceVector);
        }

        public void Jump()
        {
            if (_groundChecker.IsGrounded)
                _rigidbody.AddForce(Vector2.up * _jumpForce, ForceMode2D.Impulse);
        }

        public void Stop() => _rigidbody.velocity = _rigidbody.velocity.With(x: 0f);
    }
}