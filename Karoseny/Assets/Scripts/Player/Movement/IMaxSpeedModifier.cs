namespace ManicGames
{
    public interface IMaxSpeedModifier
    {
        float CurrentModifier { get; }
    }
}