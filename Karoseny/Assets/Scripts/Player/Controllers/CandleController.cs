using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ManicGames
{
    public class CandleController : PlayerController
    {
        [SerializeField, Required] FuelTank _fuelTank = null;
        [SerializeField, Required] ForceGroundMover _groundMover = null;
        [SerializeField, Required] CandleSlider _candleSlider = null;
        
        [SerializeField, Range(0f, 10f)] float _fuelUsage = 1f;

        void OnEnable()
        {
            AppData.InputManager.OnJumpPressed += _groundMover.Jump;
            AppData.InputManager.OnSlidePressed += _candleSlider.Slide;
        }

        void OnDisable()
        {
            _groundMover.Stop();
            AppData.InputManager.OnJumpPressed -= _groundMover.Jump;
            AppData.InputManager.OnSlidePressed -= _candleSlider.Slide;
        }

        protected override void Update()
        {
            base.Update();
            _fuelTank.UseFuel(_fuelUsage * Time.deltaTime);
        }
        void FixedUpdate() => _groundMover.Move(InputManager.MovementInputRaw);
    }
}