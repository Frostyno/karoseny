using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ManicGames
{
    public class KaroseneLampController : PlayerController
    {
        [SerializeField, Required] FuelTank _fuelTank = null;
        [SerializeField, Required] ForceGroundMover _groundMover = null;
        
        [SerializeField, Range(0f, 10f)] float _fuelUsage = 0.5f;

        void Awake() => AppData.KaroseneLamp = this;
        void OnEnable() => AppData.InputManager.OnJumpPressed += _groundMover.Jump;
        void OnDisable()
        {
            _groundMover.Stop();
            AppData.InputManager.OnJumpPressed -= _groundMover.Jump;
        }

        protected override void Update()
        {
            base.Update();
            _fuelTank.UseFuel(_fuelUsage * Time.deltaTime);
        }

        void FixedUpdate() => _groundMover.Move(InputManager.MovementInputRaw);

        public void LoadFromCheckpoint(CheckpointData checkpointData)
        {
            if (checkpointData == null) return;
            transform.position = checkpointData.Position;
            _fuelTank.LoadFromCheckpoint(checkpointData);
        }
    }
}