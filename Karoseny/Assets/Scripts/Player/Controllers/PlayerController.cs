using System;
using UnityEngine;

namespace ManicGames
{
    public class PlayerController : MonoBehaviour
    {
        protected InputManager InputManager { get; private set; } = null;

        void Start() => InputManager = AppData.InputManager;

        protected virtual void Update()
        {
            if (InputManager.HorizontalInput != 0f)
                transform.localScale = transform.localScale.With(x: -Mathf.Sign(InputManager.HorizontalInput));
        }

        public void SetControlsEnabled(bool state) => enabled = state;
    }
}