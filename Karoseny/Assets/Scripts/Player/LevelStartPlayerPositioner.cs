using UnityEngine;

namespace ManicGames
{
    public class LevelStartPlayerPositioner : MonoBehaviour
    {
        void Start()
        {
            if (AppData.KaroseneLamp != null)
                AppData.KaroseneLamp.transform.position = transform.position;
        }
    }
}
