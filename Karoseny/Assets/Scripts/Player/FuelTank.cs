using System;
using UnityEngine;

namespace ManicGames
{
    public class FuelTank : MonoBehaviour
    {
        [SerializeField, Range(0f, 1000f)] float _maxFuel = 100f;
        
        public float Fuel { get; private set; }
        public float FuelNormalized => (Fuel / _maxFuel);

        public event Action OnFuelDepleted = null;

        void Awake() => Fuel = _maxFuel;

        public void UseFuel(float amount)
        {
            if (Fuel <= 0f) return;

            Fuel = Mathf.Max(0f, Fuel - amount);
            if (Fuel <= 0f)
                OnFuelDepleted?.Invoke();
        }

        public void RefillFuel(float amount)
        {
            Fuel = Mathf.Min(_maxFuel, Fuel + amount);
        }

        public void LoadFromCheckpoint(CheckpointData data)
        {
            Fuel = data.Fuel;
        }
    }
}