using System;
using UnityEngine;

namespace ManicGames
{
    public class InputManager : MonoBehaviour
    {
        public float HorizontalInput { get; private set; } = 0f;
        public float HorizontalInputRaw { get; private set; } = 0f;

        public float VerticalInput { get; private set; } = 0f;
        public float VerticalInputRaw { get; private set; } = 0f;

        public Vector2 MovementInput { get; private set; } = Vector2.zero;
        public Vector2 MovementInputRaw { get; private set; } = Vector2.zero;

        public bool IsJumpPressed { get; private set; } = false;

        public event Action OnJumpPressed = null;
        public event Action OnSlidePressed = null;

        void Awake() => AppData.InputManager = this;

        void OnDisable() => ResetValues();

        void Update()
        {
            HorizontalInput = Input.GetAxis("Horizontal");
            HorizontalInputRaw = Input.GetAxisRaw("Horizontal");
            VerticalInput = Input.GetAxis("Vertical");
            VerticalInputRaw = Input.GetAxisRaw("Vertical");
            MovementInput = new Vector2(HorizontalInput, VerticalInput);
            MovementInputRaw = new Vector2(HorizontalInputRaw, VerticalInputRaw);

            IsJumpPressed = Input.GetKey(KeyCode.Space);
            if (Input.GetKeyDown(KeyCode.Space))
                OnJumpPressed?.Invoke();
            if (Input.GetKeyDown(KeyCode.LeftShift))
                OnSlidePressed?.Invoke();
        }

        void ResetValues()
        {
            HorizontalInput = 0f;
            HorizontalInputRaw = 0f;
            VerticalInput = 0f;
            VerticalInputRaw = 0f;
            IsJumpPressed = false;
        }
    }   
}
