using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ManicGames
{
    public class SceneLoader : MonoBehaviour
    {
        void Awake() => AppData.SceneLoader = this;

        public void LoadScene(Scene scene)
        {
            string sceneName = SceneMapper.GetSceneName(scene);
            StartCoroutine(LoadSceneAsync(sceneName));
        }

        public void UnloadScene(Scene scene)
        {
            string sceneName = SceneMapper.GetSceneName(scene);
            StartCoroutine(UnloadSceneAsync(sceneName));
        }

        public void ReloadScene(Scene scene)
        {
            string sceneName = SceneMapper.GetSceneName(scene);
            StartCoroutine(ReloadScene(sceneName));
        }

        IEnumerator ReloadScene(string sceneName)
        {
            yield return StartCoroutine(UnloadSceneAsync(sceneName));
            yield return StartCoroutine(LoadSceneAsync(sceneName));
        }

        IEnumerator LoadSceneAsync(string sceneName)
        {
            var s = SceneManager.GetSceneByName(sceneName);
            if (s.isLoaded)
            {
                Debug.LogWarning($"Scene {sceneName} is already loaded! Skipping..");
                yield break;
            }
            
            var op = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            yield return op;
        }

        IEnumerator UnloadSceneAsync(string sceneName)
        {
            var op = SceneManager.UnloadSceneAsync(sceneName);
            yield return op;
        }
    }
}