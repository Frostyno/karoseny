using System;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ManicGames
{
    public class GameManager : MonoBehaviour
    {
        CheckpointData _currentCheckpoint = null;
        FuelTank _fuelTank = null; // TODO: temporary, refactor

        public bool IsCheckpointSet => _currentCheckpoint != null;

        public event Action OnPlayerDied = null;

        Scene _currentScene = Scene.Test;

        void Awake()
        {
            AppData.GameManager = this;
        }

        void Start()
        {
            AppData.SceneLoader.LoadScene(Scene.UI);
            AppData.SceneLoader.LoadScene(Scene.Test);
            _fuelTank = AppData.KaroseneLamp.GetComponent<FuelTank>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                CutsceneController.ResumeCurrentCutscene();

            if (_fuelTank.Fuel <= 0f)
                LoadCheckpoint();
        }

        [ContextMenu("Kill Player")]
        void KillPlayer() => OnPlayerDied?.Invoke();

        public void RestartLevel() => AppData.SceneLoader.ReloadScene(_currentScene);

        public void SetCheckpoint(CheckpointData checkpoint)
        {
            if (checkpoint == null) return;
            
            _currentCheckpoint?.Checkpoint.ToggleOff();
            _currentCheckpoint = checkpoint;
        }

        public void LoadCheckpoint()
        {
            if (_currentCheckpoint == null) return;
            AppData.KaroseneLamp.LoadFromCheckpoint(_currentCheckpoint);
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
    }
}