namespace ManicGames
{
    public static class SceneMapper
    {
        public static string GetSceneName(Scene scene)
        {
            return scene switch
            {
                Scene.App => "App",
                Scene.UI => "UI",
                Scene.Test => "Test",
                _ => string.Empty
            };
        }
    }
}