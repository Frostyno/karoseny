using System;
using System.Collections;
using System.Collections.Generic;
using ManicGames;
using UnityEngine;

public class GravityScaleUpdater : MonoBehaviour
{
    [SerializeField, Range(0.5f, 10f)] float _upGravityModifier = 2f;
    [SerializeField, Range(0.5f, 10f)] float _downGravityModifier = 2f;
    
    InputManager _inputManager = null;
    Rigidbody2D _rigidbody = null;
    
    void Start()
    {
        _inputManager = AppData.InputManager;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update() => UpdateGravityScale();

    void UpdateGravityScale()
    {
        if (_rigidbody.velocity.y > 0.1f && !_inputManager.IsJumpPressed)
            _rigidbody.gravityScale = _upGravityModifier;
        else if (_rigidbody.velocity.y < -0.1f)
            _rigidbody.gravityScale = _downGravityModifier;
        else
            _rigidbody.gravityScale = 1f;
    }
}
