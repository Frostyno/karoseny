using UnityEngine;

namespace ManicGames
{
    public struct BoundsCorners
    {
        public Vector2 LeftBottom;
        public Vector2 RightBottom;
        public Vector2 LeftTop;
        public Vector2 RightTop;
    }
}