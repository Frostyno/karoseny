
using UnityEngine;

namespace ManicGames
{
    public class ImageOffseter : MonoBehaviour
    {
        SpriteRenderer _renderer = null;

        void Awake() => _renderer = GetComponent<SpriteRenderer>();

        private void Update()
        {
            var position = transform.localPosition;
            position.y = _renderer.bounds.extents.y * 0.75f;
            transform.localPosition = position;
        }
    }
}