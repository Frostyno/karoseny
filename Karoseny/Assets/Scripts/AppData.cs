namespace ManicGames
{
    public static class AppData
    {
        public static KaroseneLampController KaroseneLamp;

        public static GameManager GameManager;
        public static SceneLoader SceneLoader;
        public static InputManager InputManager;
    }
}