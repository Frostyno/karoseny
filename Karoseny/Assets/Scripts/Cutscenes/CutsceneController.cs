using UnityEngine;

namespace ManicGames
{
    public class CutsceneController : MonoBehaviour
    {
        static CutsceneController _currentCutscene = null;

        public static void ResumeCurrentCutscene()
        {
            if (_currentCutscene != null)
                _currentCutscene.ResumeCutscene();
        }

        [SerializeField] Animator _animator = null;

        bool _cutsceneViewed = false;
        bool _cutsceneRunning = false;

        readonly int _startCutsceneTriggerHash = Animator.StringToHash("StartCutscene");

        public void StartCutscene()
        {
            if (_cutsceneViewed) return;

            _cutsceneViewed = true;
            _cutsceneRunning = true;
            _currentCutscene = this;
            _animator.SetTrigger(_startCutsceneTriggerHash);
            AppData.KaroseneLamp.SetControlsEnabled(false);
        }

        void PauseCutscene()
        {
            if (!_cutsceneRunning) return;
            _animator.speed = 0f;
        }

        void ResumeCutscene() => _animator.speed = 1f;

        void EndCutscene()
        {
            if (!_cutsceneRunning) return;

            _cutsceneRunning = false;
            _currentCutscene = null;
            AppData.KaroseneLamp.SetControlsEnabled(true);
        }
    }
}