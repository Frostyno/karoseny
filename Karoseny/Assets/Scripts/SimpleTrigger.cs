using UnityEngine;
using UnityEngine.Events;

namespace ManicGames
{
    public class SimpleTrigger : MonoBehaviour
    {
        [SerializeField] UnityEvent OnAwake = null;
        [SerializeField] UnityEvent OnTriggerEntered = null;
        [SerializeField] UnityEvent OnTriggerExited = null;

        void Awake() => OnAwake?.Invoke();

        void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player"))
                return;

            OnTriggerEntered?.Invoke();
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (!other.CompareTag("Player"))
                return;

            OnTriggerExited?.Invoke();
        }
    }
}