using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace ManicGames
{
    public class Checkpoint : MonoBehaviour
    {
        static readonly int _toggleOn = Animator.StringToHash("ToggleOn");
        static readonly int _toggleOff = Animator.StringToHash("ToggleOff");

        Animator _animator = null;

        void Awake() => _animator = GetComponent<Animator>();

        void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player"))
                return;

            _animator.SetTrigger(_toggleOn);
            var data = new CheckpointData()
            {
                Position = transform.position,
                Fuel = AppData.KaroseneLamp.GetComponent<FuelTank>().Fuel,
                Checkpoint = this
            };
            AppData.GameManager.SetCheckpoint(data);
        }

        public void ToggleOff() => _animator.SetTrigger(_toggleOff);
    }

    public class CheckpointData
    {
        public Vector2 Position;
        public float Fuel;
        public Checkpoint Checkpoint;
    }
}