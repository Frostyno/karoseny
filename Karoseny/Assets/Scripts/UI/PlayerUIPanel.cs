using System;
using UnityEngine;
using UnityEngine.UI;

namespace ManicGames
{
    public class PlayerUIPanel : MonoBehaviour
    {
        [SerializeField] Slider _slider = null;

        FuelTank _fuelTank = null;
        
        void Start() => _fuelTank = AppData.KaroseneLamp.GetComponent<FuelTank>();
        void Update() => _slider.value = _fuelTank.FuelNormalized;
    }
}