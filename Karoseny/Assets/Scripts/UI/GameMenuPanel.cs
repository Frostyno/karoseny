using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace ManicGames
{
    public class GameMenuPanel : MonoBehaviour
    {
        [SerializeField] TMP_Text _gameOverText = null;
        [SerializeField] RectTransform _gameMenuPanel = null;
        [SerializeField] GameObject _gameMenu = null;
        [SerializeField] GameObject _loadCheckpointButton = null;

        void Awake()
        {
            _gameOverText.RequiredBy(this);
        }

        void Start() => AppData.GameManager.OnPlayerDied += HandlePlayerDied;

        void OnDestroy()
        {
            if (AppData.GameManager != null)
                AppData.GameManager.OnPlayerDied -= HandlePlayerDied;
        }

        void HandlePlayerDied()
        {
            _gameMenu.SetActive(true);
            _gameMenuPanel.localScale = Vector3.zero;
            _loadCheckpointButton.SetActive(AppData.GameManager.IsCheckpointSet);

            ShowGameOver();
        }

        void ShowGameOver()
        {
            var color = _gameOverText.color;
            color.a = 0f;
            _gameOverText.color = color;

            var fadeTween = DOTween.Sequence();
            fadeTween.Append(_gameOverText.DOFade(1f, 2f)).AppendInterval(2f).SetEase(Ease.InCubic);
            fadeTween.Append(_gameOverText.DOFade(0f, 1f));
            fadeTween.OnComplete(ShowGameMenu);
            fadeTween.Play();
        }

        void ShowGameMenu() => _gameMenuPanel.DOScale(1f, 0.5f).SetEase(Ease.InCubic);

        public void HideGameMenu()
        {
            _gameMenuPanel.DOScale(0f, 0.3f)
                .SetEase(Ease.InCubic)
                .OnComplete(() => _gameMenu.SetActive(false));
        }

        public void QuitGame() => AppData.GameManager.QuitGame();
        public void RestartLevel() => AppData.GameManager.RestartLevel();
        public void LoadCheckpoint() => AppData.GameManager.LoadCheckpoint();
    }
}