using System;
using UnityEngine;

public class ParallaxLayer : MonoBehaviour
{
    public float parallaxFactor;

    Vector3 _initialPosition = Vector3.zero;
    
    public void Initialize() => _initialPosition = transform.localPosition;

    public void ResetPosition() => transform.localPosition = _initialPosition;
    
    public void Move(float delta)
    {
        Vector3 newPos = transform.localPosition;
        newPos.x -= delta * parallaxFactor;
        transform.localPosition = newPos;
    }
}