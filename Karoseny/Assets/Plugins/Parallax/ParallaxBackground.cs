using System;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    ParallaxCamera _parallaxCamera;
    List<ParallaxLayer> _parallaxLayers = new List<ParallaxLayer>();
  
    void Awake()
    {
        if (_parallaxCamera == null)
            _parallaxCamera = Camera.main.GetComponent<ParallaxCamera>();
        if (_parallaxCamera != null)
            _parallaxCamera.onCameraTranslate += Move;
        SetLayers();
    }
    
    void OnEnable()
    {
        _parallaxLayers.ForEach(l => l.ResetPosition());
        Move(transform.position.x - _parallaxCamera.transform.position.x);
    }

    void OnDestroy()
    {
        if (_parallaxCamera != null)
            _parallaxCamera.onCameraTranslate -= Move;
    }

    void SetLayers()
    {
        _parallaxLayers.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            ParallaxLayer layer = transform.GetChild(i).GetComponent<ParallaxLayer>();
            
            if (layer != null)
            {
                layer.name = "Layer-" + i;
                _parallaxLayers.Add(layer);
                layer.Initialize();
            }
        }
    }
    void Move(float delta) => _parallaxLayers.ForEach(l => l.Move(delta));
}